//
// Created by ikmokhirio on 07.09.2019.
//

#include "TestController.h"

TestController::TestController(bool showAll) {

    srand(time(nullptr));

    for (int i = 0; i < testCount_; i++) {
        testResult[i] = false;
    }

    quick = bubble = shell = false;

    showAll_ = showAll;

    startTest();
}

bool TestController::createEmptySequence() {
    Sequence<int> *testSequence_old;
    //Step 1
    cout << "Creating empty sequence" << endl;
    try {
        testSequence_old = new Array<int>;
        cout << "Success" << endl;
    } catch (exception &e) {
        cout << "Fail : " << e.what() << endl;
        cout << "Critical error. Can not create sequence" << endl;
        return false;
    }
    currentSequence = testSequence_old;
    //End step 1

    cout << separator_ << endl;

    //Step 2
    cout << "Checking sequence length" << endl;
    cout << "Expected result: 0" << endl;
    cout << "Result: " << currentSequence->getLength() << endl;
    if (currentSequence->getLength() == 0) {
        cout << "Success" << endl;
    } else {
        cout << "Fail" << endl;
        return false;

    }

    cout << separator_ << endl;
    return true;

    //End step 2
}

bool TestController::testGet(int i, int expected) {

    try {
        cout << "Checking item with index [" << i << "]" << endl;
        cout << "Expection result: " << expected << endl;
        cout << "Result: " << currentSequence->get(i) << endl;

        if (currentSequence->get(i) == expected) {
            cout << "Success" << endl;
        } else {
            cout << "Fail" << endl;
            return false;
        }
    } catch (out_of_range &e) {
        cout << e.what() << endl;
        cout << "Fail" << endl;
        return false;
    }

    cout << separator_ << endl;
    return true;
}


bool TestController::checkSort(ISorter<int> *srt) {
    //Step 1
    auto *arr = new Array<int>();
    cout << "Filling sequence in ascending order" << endl;
    for (int i = 0; i < SORT_TEST_SIZE; i++) {
        arr->append(SORT_TEST_SIZE - i);
    }
    cout << "Sort in descending order" << endl;
    srt->sort(arr, [](int i, int j) {
        if (i > j) {
            return 1;
        } else if (i < j) {
            return -1;
        } else {
            return 0;
        }
    });
    cout << "Checking" << endl;
    for (int i = 0; i < SORT_TEST_SIZE - 1; i++) {
        if (arr->get(i) > arr->get(i + 1)) {
            cout << "Error!" << endl;
            return false;
        }
        if (showAll_) {
            cout << "I : " << i << "        SEQ[i] : " << arr->get(i) << endl;
        }
    }
    cout << "Success\n" << endl;
    //End step 1
    //Step 2
    cout << "Filling sequence in descending order" << endl;
    for (int i = 0; i < SORT_TEST_SIZE; i++) {
        arr->append(i);
    }
    cout << "Sort in ascending order" << endl;
    srt->sort(arr, [](int i, int j) {
        if (i > j) {
            return -1;
        } else if (i < j) {
            return 1;
        } else {
            return 0;
        }
    });
    cout << "Checking" << endl;
    for (int i = 0; i < SORT_TEST_SIZE - 1; i++) {
        if (arr->get(i) < arr->get(i + 1)) {
            cout << "Error!" << endl;
            return false;
        }
        if (showAll_) {
            cout << "I : " << i << "        SEQ[i] : " << arr->get(i) << endl;
        }
    }
    cout << "Success\n" << endl;
    //End step 2

    //Step 3
    cout << "Filling sequence with random numbers in range 1000" << endl;
    for (int i = 0; i < SORT_TEST_SIZE; i++) {
        arr->append(rand() % 1000);
    }
    cout << "Sort in ascending order" << endl;
    srt->sort(arr, [](int i, int j) {
        if (i > j) {
            return -1;
        } else if (i < j) {
            return 1;
        } else {
            return 0;
        }
    });
    cout << "Checking" << endl;
    for (int i = 0; i < SORT_TEST_SIZE - 1; i++) {
        if (arr->get(i) < arr->get(i + 1)) {
            cout << "Error!" << endl;
            return false;
        }
        if (showAll_) {
            cout << "I : " << i << "        SEQ[i] : " << arr->get(i) << endl;
        }
    }
    cout << "Success\n" << endl;
    //End step 3
    cout << separator_ << endl;
    return true;
}

void TestController::startTest() {

    testScenario_1();

    testScenario_2();

}


void TestController::testScenario_2() {
    //Sort test
    ISorter<int> *srt = nullptr;

    //Bubble
    cout << "Bubble sort check" << endl;
    srt = new BubbleSort<int>;
    bubble = checkSort(srt);
    //End bubble

    cout << separator_ << endl;

    //Shell
    cout << "Shell sort check" << endl;
    srt = new ShellSort<int>;
    shell = checkSort(srt);
    //End shell

    cout << separator_ << endl;

    //Quick
    cout << "Quick sort check" << endl;
    srt = new QuickSort<int>;
    quick = checkSort(srt);
    //End quick

    //Selection
    cout << "Selection sort check" << endl;
    srt = new QuickSort<int>;
    quick = checkSort(srt);
    //End selection

    //End sort test
}

//NEW

void TestController::testScenario_1() {

    int *testValues = new int[3]{23, 43, 53};
    int *itemIndexes = new int[3]{0, 1, 2};

    const int subsequenceFrom = 1;
    const int subsequenceTo = 1;

    testResult[0] = createEmptySequence();

    testResult[1] = appendTest(testValues[0]);

    testResult[2] = testGet(itemIndexes[0], testValues[0]);

    testResult[3] = testOutOfRange();

    testResult[4] = appendTest(testValues[1]);

    testResult[5] = testGet(itemIndexes[0], testValues[0]);

    testResult[6] = testGet(itemIndexes[1], testValues[1]);

    testResult[7] = testOutOfRange();

    testResult[8] = prependTest(testValues[2]);

    testResult[9] = testGet(itemIndexes[0], testValues[2]);

    testResult[10] = testGet(itemIndexes[1], testValues[0]);

    testResult[11] = testGet(itemIndexes[2], testValues[1]);

    testResult[12] = testOutOfRange();

    testResult[13] = testGetSubsequence(subsequenceFrom, subsequenceTo);
}

bool TestController::appendTest(int item) {

    int oldLength = 0;

    int firstItem = item;

    if (currentSequence->getLength() != 0) {
        oldLength = currentSequence->getLength();
        firstItem = currentSequence->getFirst();
    }



    //Step 1
    cout << "Adding item " << item << " to the end" << endl;
    try {
        currentSequence->append(item);
        cout << "Success" << endl;
    } catch (exception &e) {
        cout << "Fail" << endl;
        return false;
    }
    //End step 1

    cout << separator_ << endl;

    //Step 2
    cout << "Checking sequence length" << endl;
    cout << "Expected result: " << oldLength + 1 << endl;
    cout << "Result: " << currentSequence->getLength() << endl;
    if (currentSequence->getLength() == oldLength + 1) {
        cout << "Success" << endl;
    } else {
        cout << "Fail" << endl;
        return false;
    }
    //End step 2

    cout << separator_ << endl;

    //Step 3
    cout << "Checking first item" << endl;
    cout << "Expected result: " << firstItem << endl;
    cout << "Result: " << currentSequence->getFirst() << endl;
    if (currentSequence->getFirst() == firstItem) {
        cout << "Success" << endl;
    } else {
        cout << "Fail" << endl;
        return false;
    }
    //End step 3

    cout << separator_ << endl;

    //Step 4
    cout << "Checking last item" << endl;
    cout << "Expected result: " << item << endl;
    cout << "Result: " << currentSequence->getLast() << endl;
    if (currentSequence->getLast() == item) {
        cout << "Success" << endl;
    } else {
        cout << "Fail" << endl;
        return false;
    }
    //End step 4

    cout << endl;

    cout << separator_ << endl;
    return true;

}

bool TestController::prependTest(int item) {
    int oldLength = currentSequence->getLength();

    int lastItem = currentSequence->getLast();

    //Step 1
    cout << "Adding item " << item << " to the start" << endl;
    try {
        currentSequence->prepend(item);
        cout << "Success" << endl;
    } catch (exception &e) {
        cout << "Fail" << endl;
        return false;
    }
    //End step 1

    cout << separator_ << endl;

    //Step 2
    cout << "Checking sequence length" << endl;
    cout << "Expected result: " << oldLength + 1 << endl;
    cout << "Result: " << currentSequence->getLength() << endl;
    if (currentSequence->getLength() == oldLength + 1) {
        cout << "Success" << endl;
    } else {
        cout << "Fail" << endl;
        return false;
    }
    //End step 2

    cout << separator_ << endl;

    //Step 3
    cout << "Checking first item" << endl;
    cout << "Expected result: " << item << endl;
    cout << "Result: " << currentSequence->getFirst() << endl;
    if (currentSequence->getFirst() == item) {
        cout << "Success" << endl;
    } else {
        cout << "Fail" << endl;
        return false;
    }
    //End step 3

    cout << separator_ << endl;

    //Step 4
    cout << "Checking last item" << endl;
    cout << "Expected result: " << lastItem << endl;
    cout << "Result: " << currentSequence->getLast() << endl;
    if (currentSequence->getLast() == lastItem) {
        cout << "Success" << endl;
    } else {
        cout << "Fail" << endl;
        return false;
    }
    //End step 4

    cout << endl;

    cout << separator_ << endl;
    return true;

}

bool TestController::testOutOfRange() {

    //Step 1
    cout << "Checking element with index [-1] " << endl;
    cout << "Expected result: out of range error" << endl;
    try {
        currentSequence->get(-1);
        cout << "Fail" << endl;
        return false;
    } catch (std::out_of_range &e) {
        cout << "Result : Index out of range : " << e.what() << endl;
        cout << "Success" << endl;
    }
    //End step 1

    cout << endl;

    //Step 2

    cout << "Checking element with index " << currentSequence->getLength() << endl;
    cout << "Expected result: out of range error" << endl;
    try {
        currentSequence->get(currentSequence->getLength());
        cout << "Fail" << endl;
        return false;

    } catch (std::out_of_range &e) {
        cout << "Result : Index out of range : " << e.what() << endl;
        cout << "Success`" << endl;

    }
    //End step 2

    cout << separator_ << endl;
    return true;
}

bool TestController::testGetSubsequence(int i, int j) {
    int expectedLength = j - i + 1;

    int firstExpected = currentSequence->get(i);
    int lastExpected = currentSequence->get(j);

    cout << "Getting subsequence from index [" << i << "] to index [" << j << "]" << endl;
    try {
        Sequence<int> *newTestSequence = currentSequence->getSubsequence(i, j);
        cout << "Success" << endl;

        cout << endl;

        cout << "Checking subsequence length" << endl;
        cout << "Expected result: " << expectedLength << endl;
        cout << "Result: " << newTestSequence->getLength() << endl;
        if (newTestSequence->getLength() == expectedLength) {
            cout << "Success" << endl;
        } else {
            cout << "Fail" << endl;
            return false;
        }

        cout << endl;

        cout << "Checking first element" << endl;
        cout << "Expected result: " << firstExpected << endl;
        cout << "Result: " << newTestSequence->getFirst() << endl;
        if (newTestSequence->getFirst() == firstExpected) {
            cout << "Success" << endl;
        } else {
            cout << "Fail" << endl;
            return false;
        }

        cout << endl;

        cout << "Checking last element" << endl;
        cout << "Expected result: " << lastExpected << endl;
        cout << "Result: " << newTestSequence->getLast() << endl;
        if (newTestSequence->getLast() == lastExpected) {
            cout << "Success" << endl;
        } else {
            cout << "Fail" << endl;
            return false;

        }

    } catch (std::out_of_range &e) {
        cout << "Result : Index out of range : " << e.what() << endl;
        return false;
    }

    cout << separator_ << endl;
    return true;
}

void TestController::Result() {

    cout << "sequence test result" << endl;

    for (int i = 0; i < testCount_; i++) {
        if (testResult[i]) {
            cout << "Step " << i + 1 << " : success" << endl;
        } else {
            cout << "Step " << i + 1 << " : fail" << endl;
        }
    }

    cout << separator_;

    if (bubble) {
        cout << "Bubble sort test : success" << endl;
    } else {
        cout << "Bubble sort test : fail" << endl;
    }

    if (shell) {
        cout << "Shell sort test : success" << endl;
    } else {
        cout << "Shell sort test : fail" << endl;
    }

    if (quick) {
        cout << "Quick sort test : success" << endl;
    } else {
        cout << "Quick sort test : fail" << endl;
    }
}