#include <iostream>
#include "TestController.h"

#include "CommandProcessor.h"

using namespace std;

int main() {
    //Tests block
    cout << "Do you want to run tests? y/n" << endl;
    char c;
    cin >> c;
    c = tolower(c);
    if (c == 'y') {

        cout << "Do you want to print all sequences (90 000 symbols) during the test? y/n" << endl;
        char a;
        cin >> a;
        a = tolower(a);
        bool showAll = false;
        if (a == 'y') {
            showAll = true;
        } else {
            cout << "No sequences will be showed" << endl;
        }

        TestController newTest(showAll);
        newTest.Result();
    } else {
        cout << "No tests" << endl;
    }

    cin.clear();
    cout << endl << endl;

    //End tests


   
    //User input block

    char *args = new char[100];
    cin.getline(args, 100);
    CommandProcessor<int> p;

    cout << "Enter \"help\" to see command list" << endl;

    string *ans;

    while (true) {
        cout << ">";
        cin.getline(args, 100);

        for (int i = 0; i < 100; i++) {
            args[i] = tolower(args[i]);
        }

        try {
            ans = p.getCommands(args);
        } catch (length_error &e) {
            ans = nullptr;
            cerr << e.what() << endl;
        }

        if (ans == nullptr) {
            cerr << "Invalid command" << endl;
        } else if (ans[0] == "quit") {
            break;
        }

        cin.clear();

    }

    //End user input

    return 0;
}