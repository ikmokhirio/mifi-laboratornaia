#include "ISorter.h"

//
// Created by ikmokhirio on 14.09.2019.
//

#ifndef MEPHI_LAB_0_QUICKSORT_H
#define MEPHI_LAB_0_QUICKSORT_H

template<typename TElement>
class QuickSort : public ISorter<TElement> {
private:
    void quickSort(Sequence<TElement> *seq, int first, int last,
                   function<int(TElement, TElement)> compare_) {
        int i = first, j = last;
        TElement x = seq->get((first + last) / 2);
        do {
            while (compare_(seq->get(i), x) == -1) {
                i++;
            }
            while (compare_(seq->get(j), x) == 1) {
                j--;
            }

            if (i <= j) {

                if (compare_(seq->get(i), seq->get(j)) == 1) {
                    seq->swap(i, j);
                }
                i++;
                j--;
            }

        } while (i <= j);

        if (i < last)
            quickSort(seq, i, last, compare_);
        if (first < j)
            quickSort(seq, first, j, compare_);
    }

public:

    Sequence<TElement> *sort(Sequence<TElement> *seq, function<int(TElement, TElement)> compare_) {
        double startTime = clock();
        quickSort(seq, 0, seq->getLength() - 1, compare_);
        double endTime = clock();

        this->sortTime = (endTime - startTime) / CLOCKS_PER_SEC;

        return seq;
    }
};

#endif //MEPHI_LAB_0_QUICKSORT_H
