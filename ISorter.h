#include "Sequence.h"
#include <functional>
#include <ctime>

//
// Created by ikmokhirio on 14.09.2019.
//

#ifndef MEPHI_LAB_0_ISORTER_H
#define MEPHI_LAB_0_ISORTER_H

using namespace std;

template <typename TElement>
class ISorter {
protected:
    double sortTime;
public:

    ISorter() {
        sortTime = 0;
    }

    virtual Sequence<TElement> *sort(Sequence<TElement> *seq, function<int(TElement, TElement)> compare_) = 0;

    bool isSortSuccess(Sequence<TElement> *seq, function<int(TElement, TElement)> compare_) {
        int length = seq->getLength();
        for(int i = 1; i < length; i++) {
            if (compare_(seq->get(i), seq->get(i - 1)) == -1) {
                return false;
            }
        }

        return true;
    }

    double getSortTime() {
        return sortTime;
    }

};


#endif //MEPHI_LAB_0_ISORTER_H
