#include "ISorter.h"
#include <time.h>
//
// Created by ikmokhirio on 03.10.2019.
//

#ifndef MEPHI_LAB_0_BOGOSORT_H
#define MEPHI_LAB_0_BOGOSORT_H

using namespace std;

template<typename TElement>
class Bogosort : public ISorter<TElement> {
public:

    Bogosort() {
        srand(time(nullptr));
    }

    void shuffle(Sequence<TElement> *seq) {
        for (int i = 0; i < seq->getLength(); ++i)
            seq->swap(i, rand() % seq->getLength());
    }

    Sequence<TElement> *sort(Sequence<TElement> *seq, function<int(TElement, TElement)> compare_) {
        double startTime = clock();
        while (!this->isSortSuccess(seq, compare_)) {
            shuffle(seq);
        }
        double endTime = clock();

        this->sortTime = (endTime - startTime) / CLOCKS_PER_SEC;

        return seq;
    }
};

#endif //MEPHI_LAB_0_BOGOSORT_H
