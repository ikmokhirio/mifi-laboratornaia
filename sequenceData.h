#include "Array.h"
#include "List.h"
#include <iostream>

using namespace std;

//
// Created by ikmokhirio on 08.09.2019.
//

#ifndef MEPHI_LAB_0_SEQUENCEDATA_H
#define MEPHI_LAB_0_SEQUENCEDATA_H


template<typename TElement>
struct sequenceInfo {
    Sequence<TElement> *sequence;
    string name;

    sequenceInfo(Sequence<TElement> *newSequence, const string &newName) {
        sequence = newSequence;
        name = newName;
    }
};

#endif //MEPHI_LAB_0_SEQUENCEDATA_H
