#include "ISorter.h"
//
// Created by ikmokhirio on 14.09.2019.
//

#ifndef MEPHI_LAB_0_SELECTIONSORT_H
#define MEPHI_LAB_0_SELECTIONSORT_H

template<typename TElement>
class SelectionSort : public ISorter<TElement> {
public:

    Sequence<TElement> *sort(Sequence<TElement> *seq, function<int(TElement, TElement)> compare_) {
        double startTime = clock();

        int length = seq->getLength();
        int num = 0;

        for (int i = 0; i < length; i++) {
            TElement min = seq->get(i);
            num = i;
            for (int j = i; j < length; j++) {
                if (compare_(seq->get(j), min) == -1) {
                    min = seq->get(j);
                    num = j;
                }
            }
            seq->swap(i, num);
        }

        double endTime = clock();

        this->sortTime = (endTime - startTime) / CLOCKS_PER_SEC;
        return seq;
    }
};

#endif //MEPHI_LAB_0_SELECTIONSORT_H
