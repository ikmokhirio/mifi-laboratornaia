#include "sequenceData.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include "BubbleSort.h"
#include "Bogosort.h"
#include "SelectionSort.h"
#include "ShellSort.h"
#include "QuickSort.h"

using namespace std;

//
// Created by ikmokhirio on 08.09.2019.
//

#ifndef MEPHI_LAB_0_COMMANDPROCESSOR_H
#define MEPHI_LAB_0_COMMANDPROCESSOR_H

/*Create  List    TEST
 * ^       ^        ^
 * ACTION   TYPE    NAME
 *
 * Select TEST
 * ^       ^
 * ACTION Name
 *
 *
 * Get            0
 * ^              ^
 * Function      PARAM
 */

/*Commands
 * select [name]
 *
 * create [type] [name]
 * [type] - array or list
 *
 *
 *
 */

const string HELP_MSG = "Command processor for LAB 1\n\n"
                        ""
                        "help - show this message\n\n"
                        ""
                        "clear - clear screen\n\n"
                        ""
                        "quit - exit from program\n\n"
                        ""
                        "create [type] [name] random [count] [start] [end]- create sequence withe [name]\n"
                        "[type] - list or array\n"
                        ""
                        "random - optional. Uses rand to generate sequence\n"
                        "[count] - amount of items in sequence\n"
                        "[start] [end] - generate numbers between [start] and [end] numbers\n"
                        "or\n"
                        "create [type] [name] from [old name] - copy old sequence to new one\n\n"
                        ""
                        "delete [name] - delete sequence with [name]\n\n"
                        ""
                        "select [name] - select sequence with [name]\n\n"
                        ""
                        "show - show all sequences\n"
                        "or\n"
                        "show [name] - show sequence with [name]\n\n"
                        ""
                        "get [index] - get element with [index] from selected sequence\n\n"
                        ""
                        "append [element] - add [element] to the end of selected sequence\n\n"
                        ""
                        "prepend [element] - add [element] to the start of selected sequence\n\n"
                        ""
                        "first - get first element from selected sequence\n\n"
                        ""
                        "last - get last element from selected sequence\n\n"
                        ""
                        "insert [index] [element] - insert [element] to the [index] place at selected sequence\n\n"
                        ""
                        "remove [element] - remove first [element] from selected sequence\n\n"
                        ""
                        "subsequence [from] [to] [name] - get subsequence [from] [to] at selected sequence and save ot as [name]\n\n"
                        ""
                        "sort - use sort for selected sequence\n"
                        "sort [sort name] [order] time [check] out [filename]\n"
                        "available sorts: Bubble, Shell, Quick, Selection, Bogosort\n"
                        "order\n> - in ascending order\n< - in descending order\n"
                        "time: calculate time for the sort"
                        "\ncheck: manual - manually, auto - automatically"
                        "\nout [filename]"
                        "Example: >sort time auto shell out test.txt\n\n"
                        ""
                        "compare [sort_1] [sort_2] [sort_n] on [name] [type] out [file]\n"
                        "compare different types of sort on one sequence\n"
                        "*optional* output to [file]"
                        "\n\n";

enum commands {
    help,
    create,
    selectSeq,
    show,
    getItem,
    append,
    prepend,
    first, last,
    insert,
    removeItem,
    compareSeq,
    deleteSequence,
    clearScr,
    subsequence,
    sortSeq,
    noCommand
};

template<typename TElement>
class CommandProcessor {

private:
    int length_;
    int current_;
    sequenceInfo<TElement> **sequences_;

    commands currentCommand;

    //Private methods

    void showSequence(Sequence<TElement> *seq) {
        for (int i = 0; i < seq->getLength(); i++) {
            cout << i << " : " << seq->get(i) << endl;
        }
    }

    void outputSequence(ofstream &fileStream, Sequence<TElement> &seq) {
        int length_ = seq.getLength();

        for (int i = 0; i < length_; i++) {
            fileStream << i << " : " << seq.get(i) << endl;
        }
    }

    bool isNotExist(string name) {
        if (length_ == 0) { return true; }
        for (int i = 0; i < length_; i++) {
            if (sequences_[i]->name == name) {
                current_ = i;
                return false;
            }
        }
        return true;
    }

    bool creation(int argc, string *args) {

        if (argc < 3) {
            return false;
        }

        string name;
        //Detect name
        if (!args[2].empty()) {
            name = args[2];

            if (!isNotExist(name)) {
                cerr << "Was found sequence with same name!" << endl;
                return false;
            }

        } else {
            return false;
        }
        //End detect

        if (argc >= 5 && args[3] == "from") { //Copy
            string oldName = args[4];
            if (isNotExist(oldName)) {
                return false;
            }

            Sequence<TElement> *oldSeq = nullptr;

            for (int i = 0; i < length_; i++) {
                if (sequences_[i]->name == oldName) {
                    oldSeq = sequences_[i]->sequence;
                    break;
                }
            }

            if (oldSeq == nullptr) {
                return false;
            }

            if (args[1] == "list") {
                length_++;
                sequences_ = (sequenceInfo<TElement> **) realloc(sequences_,
                                                                 sizeof(sequenceInfo<TElement> *) * length_);

                sequences_[length_ - 1] = new sequenceInfo<TElement>(new List<TElement>(oldSeq), name);

            } else if (args[1] == "array") {

                length_++;

                sequences_ = (sequenceInfo<TElement> **) realloc(sequences_,
                                                                 sizeof(sequenceInfo<TElement> *) * length_);

                sequences_[length_ - 1] = new sequenceInfo<TElement>(new Array<TElement>(oldSeq), name);

            } else {
                return false;
            }

            return true;
        }

        //Creating list or array with given name
        if (args[1] == "list") {
            length_++;
            sequences_ = (sequenceInfo<TElement> **) realloc(sequences_,
                                                             sizeof(sequenceInfo<TElement> *) * length_);

            sequences_[length_ - 1] = new sequenceInfo<TElement>(new List<TElement>(), name);

        } else if (args[1] == "array") {

            length_++;

            sequences_ = (sequenceInfo<TElement> **) realloc(sequences_,
                                                             sizeof(sequenceInfo<TElement> *) * length_);

            sequences_[length_ - 1] = new sequenceInfo<TElement>(new Array<TElement>(), name);

        } else {
            return false;
        }

        if (argc >= 7 && args[3] == "random") {
            int count = 0;
            try {
                std::stringstream convert;
                convert << args[4];
                convert >> count;
            } catch (std::out_of_range &e) {
                cerr << "Error : " << e.what() << endl;
                return false;
            }

            int rangeMinus = 0;
            try {
                std::stringstream convert;
                convert << args[5];
                convert >> rangeMinus;
            } catch (std::out_of_range &e) {
                cerr << "Error : " << e.what() << endl;
                return false;
            }

            int rangePlus = 0;
            try {
                std::stringstream convert;
                convert << args[6];
                convert >> rangePlus;
            } catch (std::out_of_range &e) {
                cerr << "Error : " << e.what() << endl;
                return false;
            }

            for (int i = 0; i < count; i++) {
                sequences_[length_ - 1]->sequence->append(rangeMinus + rand() % (rangePlus - rangeMinus));
            }
        }

        cout << "Was created sequence with name : " << name << endl;
        getCommands("select " + name);

        return true;
        //End
    }

    bool selection(int argc, string *args) {

        if (argc < 2) {
            return false;
        }

        string name;
        try {
            current_ = -1;
            name = args[1];
            int i;
            for (i = 0; i < length_; i++) {
                if (sequences_[i]->name == name) {
                    current_ = i;
                    break;
                }
            }

            if (current_ != i || current_ < 0) {
                cout << "Was not found!" << endl;
                return false;
            } else {
                cout << "Was selected sequence with name : " << name << endl;
            }
        } catch (exception &e) {
            cerr << "Strange error : " << e.what() << endl;
            return false;
        }

        return true;
    }

    bool showSequences(int argc, string *args) {
        if (length_ == 0) {
            cout << "No sequences" << endl;
            return true;
        }
        for (int i = 0; i < length_; i++) {
            cout << "Sequence : " << sequences_[i]->name << " length : " << sequences_[i]->sequence->getLength();

            if (i == current_) {
                cout << " < ";
            }

            cout << endl;
        }

        cout << endl;

        if (argc >= 2) {
            if (!isNotExist(args[1])) {
                for (int i = 0; i < length_; i++) {
                    if (sequences_[i]->name == args[1]) {
                        cout << "Sequence " << sequences_[i]->name << endl << endl;
                        showSequence(sequences_[current_]->sequence);
                        return true;
                    }
                }

                return false;
            }
        }

        cout << "Selected sequence " << sequences_[current_]->name << endl;
        showSequence(sequences_[current_]->sequence);

        return true;
    }

    bool getElement(int argc, string *args) {

        if (current_ < 0 || argc < 2) {
            return false;
        }
        //Work with sequence
        int index;
        try {
            std::stringstream convert;
            convert << args[1];
            convert >> index;
        } catch (std::out_of_range &e) {
            cerr << "Error : " << e.what() << endl;
            return false;
        }


        try {
            cout << sequences_[current_]->sequence->get(index) << endl;
        } catch (std::out_of_range &e) {
            cerr << "Index out of range : " << e.what() << endl;
        }
        return true;
    }

    bool addElement(int argc, string *args) {
        if (current_ < 0 || argc < 2) {
            return false;
        }
        TElement item;

        try { //Cast from string to TElement
            std::stringstream convert;
            convert << args[1];
            convert >> item;
        } catch (exception &e) {
            cerr << "Error " << e.what() << endl;
            return false;
        }

        if (append) {
            sequences_[current_]->sequence->append(item);
        } else if (prepend) {
            sequences_[current_]->sequence->prepend(item);
        } else {
            return false;
        }

        return true;
    }

    bool getEdge(int argc, string *args) {
        if (current_ < 0 || argc < 1) {
            return false;
        }
        TElement item;
        if (first) {
            item = sequences_[current_]->sequence->getFirst();
        } else if (last) {
            item = sequences_[current_]->sequence->getLast();
        }

        cout << item << endl;
        return true;
    }

    bool insertAt(int argc, string *args) {
        if (current_ < 0 || argc < 3) {
            return false;
        }

        int index;
        try {
            std::stringstream convert;
            convert << args[1];
            convert >> index;
        } catch (exception &e) {
            cerr << "Error : " << e.what() << endl;
            return false;
        }

        TElement item;

        try { //Cast from string to TElement
            std::stringstream convert;
            convert << args[2];
            convert >> item;
        } catch (exception &e) {
            cerr << "Error " << e.what() << endl;
            return false;
        }

        try {
            sequences_[current_]->sequence->insertAt(index, item);
        } catch (exception &e) {
            cerr << "Error : " << e.what() << endl;
            return false;
        }
        return true;
    }

    bool removeElement(int argc, string *args) {
        if (current_ < 0 || argc < 2) {
            return false;
        }
        TElement item;

        try { //Cast from string to TElement
            std::stringstream convert;
            convert << args[1];
            convert >> item;
        } catch (exception &e) {
            cerr << "Error " << e.what() << endl;
            return false;
        }

        cout << "Removing : " << item << endl;

        sequences_[current_]->sequence->remove(item);
        return true;
    }

    bool getSubsequence(int argc, string *args) {
        if (current_ < 0 || argc < 4) {
            return false;
        }
        int i, j;
        try {
            std::stringstream convert;
            convert << args[1];
            convert >> i;
            std::stringstream convert_2;
            convert_2 << args[2];
            convert_2 >> j;
        } catch (exception &e) {
            cerr << "Error : " << e.what() << endl;
            return false;
        }

        Sequence<TElement> *newSeq = sequences_[current_]->sequence->getSubsequence(i, j); //What next???

        length_++;
        sequences_ = (sequenceInfo<TElement> **) realloc(sequences_,
                                                         sizeof(sequenceInfo<TElement> *) * length_);

        sequences_[length_ - 1] = new sequenceInfo<TElement>(newSeq, args[3]);

        cout << "Was created sequence with name : " << args[3] << endl;
        getCommands("select " + args[3]);

        return true;
    }

    bool sortSequence(int argc, string *args) { //sort [Sort_name] [Order] [Time (y/n)] [Check (a/m)] out [file]

        if (current_ < 0 || argc < 2) {
            return false;
        }

        function<int(TElement, TElement)> f;

        bool setOrder = false;
        bool automatically = false;
        bool time = false;
        bool setName = false;

        ISorter<TElement> *srt;

        string sortName;


        ofstream *fileOut = nullptr;

        int index = 0;
        for (int i = 0; i < argc; i++) {
            if (args[i] == "out") {
                index = i;
                fileOut = new ofstream(args[index + 1], std::ios::out); //File to out
            }
            if (args[i] == ">") {
                f = [](int i, int j) {
                    if (i > j) {
                        return 1; //You can change it to -1
                    } else if (i < j) {
                        return -1; //You can change it to 1
                    } else if (i == j) {
                        return 0;
                    }
                    throw std::logic_error("Unexpected error!");
                };

                setOrder = true;
            } else if (args[i] == "<") {
                f = [](int i, int j) {
                    if (i > j) {
                        return -1; //You can change it to -1
                    } else if (i < j) {
                        return 1; //You can change it to 1
                    } else if (i == j) {
                        return 0;
                    }
                    throw std::logic_error("Unexpected error!");
                };
                setOrder = true;
            }

            if (args[i] == "time") {
                time = true;
            }

            if (args[i] == "auto") {
                automatically = true;
            } else if (args[i] == "manual") {
                automatically = false;
            }


            if (args[i] == "bubble") {
                sortName = args[i];
                srt = new BubbleSort<TElement>();
                setName = true;
            } else if (args[i] == "shell") {
                sortName = args[i];
                srt = new ShellSort<TElement>();
                setName = true;
            } else if (args[i] == "quick") {
                sortName = args[i];
                srt = new QuickSort<TElement>();
                setName = true;
            } else if (args[i] == "selection") {
                sortName = args[i];
                srt = new SelectionSort<TElement>();
                setName = true;
            } else if (args[i] == "bogosort") {
                sortName = args[i];
                srt = new Bogosort<TElement>();
                setName = true;
            }
        }

        if (fileOut != nullptr) {
            *fileOut << "Sorting start\n" << endl;
        }

        if (!setOrder) { //Default order
            f = [](int i, int j) {
                if (i > j) {
                    return 1; //You can change it to -1
                } else if (i < j) {
                    return -1; //You can change it to 1
                } else if (i == j) {
                    return 0;
                }
                throw std::logic_error("Unexpected error!");
            };
        }

        if (!setName) {
            return false;
        }

        if (fileOut != nullptr) {
            *fileOut << "Using " << sortName << " sort\n\nSequence:" << endl;
            outputSequence(*fileOut, *sequences_[current_]->sequence);
            *fileOut << endl;
        }


        cout << "Starting " << args[1] << " sort" << endl;

        srt->sort(sequences_[current_]->sequence, f);

        if (time) {
            cout << "Sort time : " << srt->getSortTime() << endl;
        }

        if (fileOut != nullptr) {
            *fileOut << "Sort time : " << srt->getSortTime() << endl;
        }

        if (automatically) {
            if (srt->isSortSuccess(sequences_[current_]->sequence, f)) {
                cout << "Automatically check result success" << endl;
                if (fileOut != nullptr) {
                    *fileOut << "Automatically check result success" << endl;
                }
            } else {
                if (fileOut != nullptr) {
                    *fileOut << "Automatically check result fail" << endl;
                }
                cout << "Automatically check result fail" << endl;
            }
        } else {
            cout << "You can manually check sort result" << endl;
            showSequence(sequences_[current_]->sequence);

            if (fileOut != nullptr) {
                *fileOut << "Sorted sequence:" << endl;
                outputSequence(*fileOut, *sequences_[current_]->sequence);
                *fileOut << endl;

                cout << "All data was writen to " << args[index + 1] << endl;
            }

        }

        return true;
    }

    bool compare(int argc, string *args) { //Check copy and sort????
        if (argc < 2) {
            return false;
        }

        bool bubble = false;
        bool shell = false;
        bool quick = false;
        bool selection = false;
        bool bogosort = false;

        bool array = true;

        string fileName;

        for (int i = 0; i < argc; i++) {
            if (args[i] == "bubble") {
                bubble = true;
            } else if (args[i] == "shell") {
                shell = true;
            } else if (args[i] == "quick") {
                quick = true;
            } else if (args[i] == "selection") {
                selection = true;
            } else if (args[i] == "bogosort") {
                bogosort = true;
            }
            if (args[i] == "out" && i < argc - 1) {
                fileName = args[i + 1];
            }

            if (args[i] == "on" && i < argc - 2) {
                getCommands({"select" + args[i + 1]});
                if (args[i + 2] == "array") {
                    array = true;
                } else if (args[i + 2] == "list") {
                    array = false;
                } else {
                    return false;
                }
            }
        }
        if (bubble) {
            if (array) {
                getCommands("create array test_1 from " + sequences_[current_]->name);
            } else {
                getCommands("create list test_1 from " + sequences_[current_]->name);
            }
            getCommands("select test_1");
            getCommands("sort bubble time manual out " + fileName + "_bubble");
            getCommands("delete test_1");
        }
        if (shell) {
            if (array) {
                getCommands("create array test_1 from " + sequences_[current_]->name);
            } else {
                getCommands("create list test_1 from " + sequences_[current_]->name);
            }
            getCommands("select test_1");
            getCommands("sort shell time manual out " + fileName + "_shell.txt");
            getCommands("delete test_1");
        }
        if (quick) {
            if (array) {
                getCommands("create array test_1 from " + sequences_[current_]->name);
            } else {
                getCommands("create list test_1 from " + sequences_[current_]->name);
            }
            getCommands("select test_1");
            getCommands("sort quick time manual out " + fileName + "_quick.txt");
            getCommands("delete test_1");
        }
        if (selection) {
            if (array) {
                getCommands("create array test_1 from " + sequences_[current_]->name);
            } else {
                getCommands("create list test_1 from " + sequences_[current_]->name);
            }
            getCommands("select test_1");
            getCommands("sort selection time manual out " + fileName + "_selection.txt");
            getCommands("delete test_1");
        }
        if (bogosort) {
            if (array) {
                getCommands("create array test_1 from " + sequences_[current_]->name);
            } else {
                getCommands("create list test_1 from " + sequences_[current_]->name);
            }
            getCommands("sort bogosort time manual out " + fileName + "_bogosort.txt");
            getCommands("delete test_1");
        }

        return true;
    }

    bool deleteSeq(int argc, string *args) {
        if (argc < 2) {
            return false;
        }
        for (int i = 0; i < length_; i++) {
            if (sequences_[i]->name == args[1]) {
                delete sequences_[i];
                for (int j = length_ - 1; j > i; j--) {
                    sequences_[j - 1] = sequences_[j];
                }
                length_--;
                sequences_ = (sequenceInfo<TElement> **) realloc(sequences_,
                                                                 sizeof(sequenceInfo<TElement> *) * length_);

                if (current_ == i && length_ > 0) {
                    getCommands("select " + sequences_[0]->name);
                }
                return true;
            }
        }
        return false;
    }

    void showHelp() {
        cout << HELP_MSG << endl;
    }

    void clearScreen() {
        system("cls");
    }


    bool executeCommand(int argc, string *args) { //True if command was successfully detected

        switch (currentCommand) {
            case (help): {
                showHelp();
                break;
            }
            case (create) : { //Create block
                if (!creation(argc, args)) {
                    return false;
                }
                break;
            }
            case (selectSeq) : { //Select block
                if (!selection(argc, args)) {
                    return false;
                }
                break;
            }
            case (show): { //Show all sequences or only one
                showSequences(argc, args);
                break;
            }
            case (getItem) : { //Getting element from sequence
                if (!getElement(argc, args)) {
                    return false;
                }
                break;
            }
            case (append) : { //Adding element to sequence
                if (!addElement(argc, args)) {
                    return false;
                }
                break;
            }
            case (prepend) : { //Adding element to sequence
                if (!addElement(argc, args)) {
                    return false;
                }
                break;
            }
            case (first) : {
                if (!getEdge(argc, args)) {
                    return false;
                }
                break;
            }
            case (last) : {
                if (!getEdge(argc, args)) {
                    return false;
                }
                break;
            }
            case (insert) : { // insertAt index item
                if (!insertAt(argc, args)) {
                    return false;
                }
                break;
            }
            case (removeItem) : {
                if (!removeElement(argc, args)) {
                    return false;
                }
                break;
            }
            case (subsequence) : {
                if (!getSubsequence(argc, args)) {
                    return false;
                }
                break;
            }
            case (sortSeq) : {
                if (!sortSequence(argc, args)) {
                    return false;
                }
                break;
            }
            case (clearScr) : {
                clearScreen();
                break;
            }
            case (compareSeq) : {
                if (!compare(argc, args)) {
                    return false;
                }
                break;
            }
            case (deleteSequence) : {
                if (!deleteSeq(argc, args)) {
                    return false;
                }
                break;
            }
            default: {
                return false;
                break;
            }
        }
        return true; //Success
    }

    //End

public:
    CommandProcessor() {

        srand(time(nullptr));

        length_ = 0;
        sequences_ = (sequenceInfo<TElement> **) malloc(sizeof(sequenceInfo<TElement>));
        current_ = 0;

        currentCommand = noCommand;
    }

    string *getCommands(const string &commandList) {
        const char *charString = commandList.c_str(); //Get string as array of char
        int count = 0;

        for (int i = 0; charString[i] != '\0'; i++) {
            if (charString[i] == ' ') {
                count++; //Count - amount of whitespaces
            }

        }

        count++;

        stringstream buffStream;
        buffStream << charString;

        auto *commandsList = new string[count];

        for (int i = 0; i < count; i++) {
            buffStream >> commandsList[i]; //Get every word in a string
        }

        if (commandsList[0].length() == 0) {
            throw std::length_error("Empty string was passed");
        }

        if (commandsList[0] == "help") {
            currentCommand = help;
        } else if (commandsList[0] == "create") { //Create block
            currentCommand = create;
        } else if (commandsList[0] == "select") { //Select block
            currentCommand = selectSeq;
        } else if (commandsList[0] == "show") { //Show all sequences or only one
            currentCommand = show;
        } else if (commandsList[0] == "get") { //Getting element from sequence
            currentCommand = getItem;
        } else if (commandsList[0] == "append") { //Adding element to sequence
            currentCommand = append;
        } else if (commandsList[0] == "prepend") {
            currentCommand = prepend;
        } else if (commandsList[0] == "first") {
            currentCommand = first;
        } else if (commandsList[0] == "last") {
            currentCommand = last;
        } else if (commandsList[0] == "insert") { // insertAt index item
            currentCommand = insert;
        } else if (commandsList[0] == "remove") {
            currentCommand = removeItem;
        } else if (commandsList[0] == "subsequence") {
            currentCommand = subsequence;
        } else if (commandsList[0] == "sort") {
            currentCommand = sortSeq;
        } else if (commandsList[0] == "clear") {
            currentCommand = clearScr;
        } else if (commandsList[0] == "compare") {
            currentCommand = compareSeq;
        } else if (commandsList[0] == "delete") {
            currentCommand = deleteSequence;
        } else {
            currentCommand = noCommand;
        }

        if (commandsList[0] != "quit" && !executeCommand(count, commandsList)) {
            return nullptr;
        }

        return commandsList;
    }

};


#endif //MEPHI_LAB_0_COMMANDPROCESSOR_H
