#include "List.h"
#include "Array.h"
#include "BubbleSort.h"
#include "ShellSort.h"
#include "QuickSort.h"

#include <iostream>

using namespace std;

//
// Created by ikmokhirio on 07.09.2019.
//

#ifndef MEPHI_LAB_0_TESTCONTROLLER_H
#define MEPHI_LAB_0_TESTCONTROLLER_H


class TestController {
private:

    Sequence<int> *currentSequence;

    bool showAll_;

    static const int SORT_TEST_SIZE = 10000;

    static const int testCount_ = 14;

    bool *testResult = new bool[testCount_];

    bool quick, bubble, shell;

    const string separator_ = "\n============================\n";

    void startTest();

    bool appendTest(int item);

    bool prependTest(int item);

    bool testOutOfRange();

    bool testGetSubsequence(int i, int j);

    bool testGet(int i, int expected);

    void testScenario_1();

    void testScenario_2();

    bool createEmptySequence();

    bool checkSort(ISorter<int> *srt);

public:

    explicit TestController(bool showAll);

    void Result();

};


#endif //MEPHI_LAB_0_TESTCONTROLLER_H
